package com.example.android.sms;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Struct;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    boolean addchek;
    DbManager dbManager;
    final ArrayList<words> word = new ArrayList<words>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbManager=new DbManager(this);
        getSMSCOnversationlist();
        final ArrayList<com.example.android.sms.words> word=new ArrayList<com.example.android.sms.words>();
    }

    ArrayList<String> name=new ArrayList<>();


    private void getSMSCOnversationlist() {
        Uri SMS_INBOX = Uri.parse("content://sms/conversations/");
        Cursor c = getContentResolver().query(SMS_INBOX, null, null, null, "date desc");

        startManagingCursor(c);
        String[] count = new String[c.getCount()];
        String[] snippet = new String[c.getCount()];
        String[] thread_id = new String[c.getCount()];
        String[] date= new String[c.getCount()];
       // String[] smsDate= new String[c.getCount()];
        c.moveToFirst();
        for (int i = 0; i < c.getCount(); i++) {
            count[i] = c.getString(c.getColumnIndexOrThrow("msg_count"))
                    .toString();
            thread_id[i] = c.getString(c.getColumnIndexOrThrow("thread_id"))
                    .toString();
            snippet[i] = c.getString(c.getColumnIndexOrThrow("snippet"))
                    .toString();
          //  date[i] = c.getString(c.getColumnIndexOrThrow("date")).toString();
            //Long timestamp = Long.parseLong(date[i]);
            //Calendar calendar = Calendar.getInstance();
            //calendar.setTimeInMillis(timestamp);
            //Date finaldate = calendar.getTime();
            //smsDate[i]= finaldate.toString();
            //Toast.makeText(getApplicationContext(), count[i] + " - " + thread_id[i]+" - "+snippet[i] , Toast.LENGTH_LONG).show();
            c.moveToNext();
        }
        c.close();
        //to get address,ie,phno
        int k = thread_id.length;
        ArrayList<String> number = new ArrayList<String>();
        for (int ad = 0; ad < thread_id.length; ad++) {
            Uri uri = Uri.parse("content://sms/inbox");
            String where = "thread_id=" + thread_id[ad];
            Cursor mycursor = getContentResolver().query(uri, null, where, null, null);
            startManagingCursor(mycursor);

            if (mycursor.moveToFirst()) {
                for (int i = 0; i < mycursor.getCount(); i++) {
                    number.add(mycursor.getString(mycursor.getColumnIndexOrThrow("address")).toString());

                    mycursor.moveToNext();
                }
            }
            mycursor.close();
        }

        //finally checking the adresses (if in contact list) and adding to a list
        for (int i = 0; i < k; i++) {


            String a = number.get(i).substring(0, 1);


            if (!a.equals("+")) {
                name.add(number.get(i));
            }


            ContentResolver localContentResolver = getApplicationContext().getContentResolver();
            Cursor contactLookupCursor =
                    localContentResolver.query(
                            Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                                    Uri.encode(number.get(i))),
                            new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID},
                            null,
                            null,
                            null);
            try {
                while (contactLookupCursor.moveToNext()) {
                    String contactName = contactLookupCursor.getString(contactLookupCursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup.DISPLAY_NAME));
                    name.add(contactName);

                }
            } catch (Exception e) {
                name.add(number.get(i).toString());

            } finally {

                //contactLookupCursor.close();
            }

        }

        for (int j = 0; j < k; j++) {
            addchek=dbManager.addData(name.get(j),snippet[j]);
            /*
        word.add(new words(name.get(j), snippet[j]));}
        Wordadapter adapter = new Wordadapter(this, 0, word);
        listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(adapter);
        */
        }
        Cursor data=dbManager.readData();
        data.moveToFirst();
        do{
            word.add(new words(data.getString(0),data.getString(1)));
        }while(data.moveToNext());
        Wordadapter adapter=new Wordadapter(this,0,word);
        ListView listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(adapter);
    }
}
