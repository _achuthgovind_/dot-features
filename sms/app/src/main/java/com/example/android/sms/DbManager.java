package com.example.android.sms;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

class DbManager extends SQLiteOpenHelper {
    public static final int Version = 1;
    public static final String TABLE_NAME = "smsmanager";
    public static final String COLUMN_NAME_NAME = "customerName";
    //public static final String COLUMN_NAME_CALLTYPE = "sentType";
    public static final String COLUMN_NAME_MESSAGE = "Message";
    //public static final String COLUMN_NAME_REMINDER = "Reminder SET?";
    public static final String COLUMN_NAME_DATE = "Date";
    //public static final String COLUMN_NAME_DURATION = "Duration";
    //public static final String number= "PhoneNumber";
    public static final String Create_Entry = "CREATE TABLE "+ TABLE_NAME + " ( " +
            COLUMN_NAME_NAME + " TEXT,"+
            //COLUMN_NAME_CALLTYPE+" TEXT,"+
            COLUMN_NAME_MESSAGE+" TEXT"+
            //COLUMN_NAME_REMINDER+" BOOLEAN,"+
           // COLUMN_NAME_DATE+" TEXT "+
            //COLUMN_NAME_DURATION+" TEXT, "+
            //number+" TEXT"+
            ");";
    public static final String Drop_Entry = "DROP TABLE IF EXISTS "+ TABLE_NAME;
    public DbManager (Context context){
        super(context,TABLE_NAME,null,Version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(Create_Entry);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Drop_Entry);
        onCreate(db);
    }



    public boolean addData(String name,String Message){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_NAME, name);
        values.put(COLUMN_NAME_MESSAGE, Message);
        //values.put(COLUMN_NAME_DATE,date);
        long result = db.insert(TABLE_NAME,null,values);
        if (result==-1)
            return false;
        else
            return true;
    }
    public Cursor readData(){
        SQLiteDatabase database=this.getReadableDatabase();
        String[] projection = {
                DbManager.COLUMN_NAME_NAME,
                DbManager.COLUMN_NAME_MESSAGE
                //DbManager.COLUMN_NAME_DATE,


        };
        Cursor data=database.query(
                this.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );
        return data;
    }
}

