package com.example.android.sms;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class Wordadapter extends ArrayAdapter<words> {

    public Wordadapter(@NonNull Context context, int resource, @NonNull List<words> word) {
        super(context, 0, word);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Check if the existing view is being reused, otherwise inflate the view
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.smsconvo, parent, false);
        }

        // Get the object located at this position in the list
        words currentWord = getItem(position);
        TextView cname = (TextView) listItemView.findViewById(R.id.customername);
        cname.setText(currentWord.getcustomer());
        TextView ctype = (TextView) listItemView.findViewById(R.id.body);
        ctype.setText(currentWord.getbody());
       // TextView date=(TextView) listItemView.findViewById(R.id.date);
       //date.setText(currentWord.getDate());
        return listItemView;
    }
}

