package com.example.android.cam;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.ArrayList;

public class ReminderContents extends AppCompatActivity {
    ReminderDb db;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.allreminders);
        db = new ReminderDb(this);
        final ArrayList<words> word=new ArrayList<words>();
        Cursor data=db.readData();
        data.moveToFirst();
        do{
            word.add(new words(data.getString(0),data.getString(2),data.getLong(3),data.getString(1),data.getString(4)));
        }while(data.moveToNext());
        Wordadapter adapter=new Wordadapter(this,0,word);
        ListView listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(adapter);
    }
}
