package com.example.android.cam;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.w3c.dom.Text;

public class ReminderDb extends SQLiteOpenHelper {
    public static final int Version = 1;
    public static final String TABLE_NAME ="reminder";
    public static final String CustomerName = "CustomerName";
    public static final String reminderText = "ReminderText";
    public static final String alarmtime = "AlarmTime";
    public static final String createQuery =
            "CREATE TABLE " + TABLE_NAME +
            " ( " + CustomerName +
            " Text," + reminderText +
            " Text,"+ alarmtime + " long);";
    public static final String Drop_Entry = "DROP TABLE IF EXISTS "+ TABLE_NAME;
    public ReminderDb (Context context){
        super(context,TABLE_NAME,null,Version);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createQuery);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Drop_Entry);
        onCreate(db);
    }
    public boolean addData(String name,String body,Long time){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CustomerName, name);
        values.put(reminderText, body);
        values.put(alarmtime, time);
        long result = db.insert(TABLE_NAME,null,values);
        if (result==-1)
            return false;
        else
            return true;
    }
    public Cursor readData(){
        SQLiteDatabase database=this.getReadableDatabase();
        String[] projection = {
                ReminderDb.CustomerName,
                ReminderDb.reminderText,
                ReminderDb.alarmtime
        };
        Cursor data=database.query(
                this.TABLE_NAME,
                projection ,
                null,
                null,
                null,
                null,
                null
        );
        return data;
    }
    public Cursor readData(long time) {
        SQLiteDatabase database=this.getReadableDatabase();
        String[] projection = {
                ReminderDb.CustomerName,
                ReminderDb.reminderText,
                ReminderDb.alarmtime
        };
        Cursor data = database.query(
                this.TABLE_NAME,
                projection,
                alarmtime + " =? ",
                new String[]{String.valueOf(time)},
                null,
                null,
                null
        );
        return data;
    }
}
