package com.example.android.cam;

public class words {
    final String wcustomer;
    final String wduration;
    final long wdate;
    final String wtype;
    final String wnumber;
    words(String customer,String number,long date,String type,String duration)
    {
        wcustomer=customer;
        wduration=duration;
        wdate=date;
        wtype=type;
        wnumber=number;
    }
    String getWcustomer(){return wcustomer;}
    String getWduration(){return wduration;}
    long getWdate(){return wdate;}
    String getWtype(){return wtype;}
    String getWnumber(){return wnumber;}
}
