package com.example.android.cam;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.nfc.Tag;
import android.util.Log;

import static java.util.Spliterator.DISTINCT;

class DbManager extends SQLiteOpenHelper {
    public static final int Version = 1;
    public static final String TABLE_NAME = "callmanager";
    public static final String COLUMN_NAME_NAME = "callerName";
    public static final String COLUMN_NAME_CALLTYPE = "callType";
    //public static final String COLUMN_NAME_MESSAGE = "Message";
    //public static final String COLUMN_NAME_REMINDER = "Reminder SET?";
    public static final String COLUMN_NAME_DATE = "Date";
    public static final String COLUMN_NAME_DURATION = "Duration";
    public static final String number= "PhoneNumber";
    public static final String Create_Entry = "CREATE TABLE "+ TABLE_NAME + " ( " +
            COLUMN_NAME_NAME + " TEXT,"+
            COLUMN_NAME_CALLTYPE+" TEXT,"+
            number+" TEXT,"+
            COLUMN_NAME_DATE+" REAL,"+
            COLUMN_NAME_DURATION+" TEXT "+
            ");";
    public static final String Drop_Entry = "DROP TABLE IF EXISTS "+ TABLE_NAME;
    //public static final String deleteduplicate = "DELETE FROM customername where rowid NOT IN (select min(rowid) from dist group by Date,callername);";
    public DbManager (Context context){
        super(context,TABLE_NAME,null,Version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(Create_Entry);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Drop_Entry);
        onCreate(db);
    }



    public boolean addData(String name,String duration,long date,String number,String type){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_NAME, name);
        values.put(COLUMN_NAME_CALLTYPE, type);
        values.put(COLUMN_NAME_DATE,date);
        values.put(this.number,number);
        values.put(COLUMN_NAME_DURATION, duration);
        long result = db.insert(TABLE_NAME,null,values);
        if (result==-1)
            return false;
        else
            return true;
    }
    public Cursor readData(){
        SQLiteDatabase database=this.getReadableDatabase();
        String[] projection = {
                DbManager.COLUMN_NAME_NAME,
                DbManager.COLUMN_NAME_CALLTYPE,
                DbManager.number,
                DbManager.COLUMN_NAME_DURATION,
                DbManager.COLUMN_NAME_DATE
        };
        Cursor data=database.query(
                true,
                this.TABLE_NAME,
                projection ,
                null,
                null,
                //COLUMN_NAME_DATE,
                null,
                null,
                COLUMN_NAME_DATE+" DESC",
                null
        );
        return data;
    }


}

