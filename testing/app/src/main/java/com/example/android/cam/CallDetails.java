package com.example.android.cam;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.telecom.Call;
import android.util.Log;

import java.util.Date;

public class CallDetails {
    String phoneno,cname,cduration,ctype;
    long cdate;
    void setCallerName(String callername){
        cname=callername;
    }
    void setCallerNumber(String phnumber){
        phoneno=phnumber;
    }
    void setCallDuration(String duration){
        cduration=duration;
    }
    void setCallType(String type){
        ctype=type;
    }
    void setCallTimeStamp(long date){
        cdate=date;
    }
    public static String getContactName(final String phoneNumber, Context context)
    {
        Uri uri=Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,Uri.encode(phoneNumber));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};

        String contactName="";
        Cursor cursor=context.getContentResolver().query(uri,projection,null,null,null);

        if (cursor != null) {
            if(cursor.moveToFirst()) {
                contactName=cursor.getString(0);
            }
            cursor.close();
        }

        return contactName;
    }
    private static String timeConversion(int totalSeconds) {
        int hours = totalSeconds /3600;
        int minutes = (totalSeconds - (hoursToSeconds(hours)))
                / 60;
        int seconds = totalSeconds
                - ((hoursToSeconds(hours)) + (minutesToSeconds(minutes)));

        return hours + " h " + minutes + " m " + seconds + " s";
    }

    private static int hoursToSeconds(int hours) {
        return hours * 60 * 60;
    }

    private static int minutesToSeconds(int minutes) {
        return minutes * 60;
    }
    public static CallDetails getLastCallDetails(Context context) {
        String attended;

        CallDetails callDetails = new CallDetails();

        Uri contacts = CallLog.Calls.CONTENT_URI;
        try {

            Cursor managedCursor = context.getContentResolver().query(contacts, null, null, null, android.provider.CallLog.Calls.DATE + " DESC limit 1;");

            int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
            int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
            int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
            int type = managedCursor.getColumnIndex(String.valueOf(CallLog.Calls.TYPE));

            if(managedCursor.moveToFirst()){ // added line
            //if (managedCursor.getPosition())
                do{
                    String callType;
                    String phNumber = managedCursor.getString(number);
                    String callerName = getContactName( phNumber,context);
                    attended=CustomPhoneStateListener.getAttended();
                    int inctype=managedCursor.getInt(type);
                    if(inctype == 1){
                        callType = "incmng";

                    }
                    else if (inctype == 2){
                        callType = "outgoing";
                        }
                        else if (inctype == 3){
                        callType = "Missed";
                    }
                    else if (inctype == 4){
                        callType = "voicemail";
                    }
                    else if (inctype == 5)
                    {
                        callType = "Rejected";
                    }
                    else callType="refused list";
                   // String callDate = managedCursor.getString(date);
                   // String callDayTime = new Date(Long.valueOf(callDate)).toString();
                    long callDayTime = managedCursor.getLong(date);
                    String callDuration = managedCursor.getString(duration);
                    int totseconds=Integer.valueOf(callDuration);
                    String durationfinal = timeConversion(totseconds);
                    callDetails.setCallerName(callerName);
                    callDetails.setCallerNumber(phNumber);
                    callDetails.setCallDuration(durationfinal);
                    callDetails.setCallType(callType);
                    callDetails.setCallTimeStamp(callDayTime);

                }while (managedCursor.moveToNext());
            }
            managedCursor.close();

        } catch (SecurityException e) {
            Log.e("Security Exception", "User denied call log permission");

        }

        return callDetails;

    }
}

