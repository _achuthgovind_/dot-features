package com.example.android.cam;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class CustomPhoneStateListener extends PhoneStateListener {
    public static String notattended="na";
    public static String attended="na";
    public static String temp;
    int prev_state;
    //private static final String TAG = "PhoneStateChanged";
    Context context; //Context to make Toast if required
    public CustomPhoneStateListener(Context context) {
        super();
        this.context = context;
    }

    @Override
    public void onCallStateChanged(int state, String incomingNumber) {
        super.onCallStateChanged(state, incomingNumber);

            switch (state) {
            case TelephonyManager.CALL_STATE_IDLE:
                if ((prev_state == TelephonyManager.CALL_STATE_OFFHOOK)) {
                    prev_state = state;
                    Toast.makeText(context, "Call End", Toast.LENGTH_SHORT).show();
                    context.startActivity(new Intent(context,ConversationActivity.class));
                    //Answered Call which is ended
                }
                if ((prev_state == TelephonyManager.CALL_STATE_RINGING)) {
                    prev_state = state;
                    //Rejected or Missed call
                }
                break;
            case TelephonyManager.CALL_STATE_OFFHOOK:
                //when Off hook i.e in call
                //Make intent and start your service here
                //Intent intent= new Intent();
                prev_state = state;
                context.startService(new Intent(context, FloatingViewService.class));
              // context.startActivity(new Intent(context,ConversationActivity.class));
                //Toast.makeText(context, "Call ended (Phone state)", Toast.LENGTH_SHORT).show();
                attended="A";
                break;
            case TelephonyManager.CALL_STATE_RINGING:
                prev_state = state;
                break;
            default:
                break;
        }
    }
    public static String getAttended(){
        if(attended.equalsIgnoreCase("A"))
        {temp=attended;
        attended="na";
        return temp;
        }
        else
            return notattended;
    }

}
