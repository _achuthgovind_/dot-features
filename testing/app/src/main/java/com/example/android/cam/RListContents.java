package com.example.android.cam;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.ArrayList;

public class RListContents extends AppCompatActivity{
    ReminderDb db;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.allreminders);
        db = new ReminderDb(this);
        final ArrayList<rwords> rword=new ArrayList<rwords>();
        Cursor data=db.readData();
        data.moveToFirst();
        do{
            rword.add(new rwords(data.getString(0),data.getString(1)));
        }while(data.moveToNext());
        RWordAdapter adapter=new RWordAdapter(this,0,rword);
        ListView listView = (ListView) findViewById(R.id.ReminderList);
        listView.setAdapter(adapter);
    }

}
