package com.example.android.cam;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.cam.utils.Constant;

public class ConversationActivity extends AppCompatActivity {

    private View mFloatingView;
    String business = " a business call?", inc = "was the last call by", out = "was the call to";
    String que = "was the call to ";
    String finalstring;
    boolean addcheck;
    DbManager dbManager;
    TextView name;
    ReminderDb dbReminder;
    RelativeLayout reminderalert ;
    TextView notify ;
    Cursor data;
    View reminderview;
    BroadcastReceiver br;
    String temp,remindertext;
    //permission for call
    public  boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG","Permission is granted");
                return true;
            } else {

                Log.v("TAG", "Permission is revoked");
                 ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG","Permission is granted");
            return true;
        }
    }



    //ConversationActivity ca=new ConversationActivity();
    public ConversationActivity() {
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);
        //Inflate the floating view layout we created
       mFloatingView = LayoutInflater.from(this).inflate(R.layout.activity_conversation, null);
        final EditText smsbody = (EditText)findViewById(R.id.textentry);
        final EditText reminderbody = (EditText)findViewById(R.id.remindertext);
        //The root element of the expanded view layout
        final View expandedView = findViewById(R.id.expanded_container);
        final View smsview = findViewById(R.id.smstextbox);
        reminderview = findViewById(R.id.reminderbox);
        expandedView.setVisibility(View.VISIBLE);
        //Set the view while floating view is expanded.
//Set the play button.
        Button noButton = (Button) findViewById(R.id.no_btn);
        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(FloatingViewService.this, "selected no.", Toast.LENGTH_LONG).show();
                smsview.setVisibility(View.VISIBLE);
                expandedView.setVisibility(View.GONE);
                // collapsedView.setVisibility(View.VISIBLE);
            }});

        Button sendbutton = (Button) this.findViewById(R.id.finalsend);
        sendbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cursor data = dbManager.readData();
                data.moveToLast();
                String body = smsbody.getText().toString();
                sendSMS(data.getString(2),body);
                Toast.makeText(getApplicationContext(), "message sent", Toast.LENGTH_LONG).show();
                smsbody.setText("");
                /*here i can send message to any number.
                 */
            }
        });
        Button cancelbutton = (Button) findViewById(R.id.cancel);
        cancelbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //collapsedView.setVisibility(View.VISIBLE);
                //startService(new Intent(ConversationActivity.this,FloatingViewService.class));
                smsview.setVisibility(View.GONE);
                reminderview.setVisibility(View.VISIBLE);
            }
        });
        Button SetReminderButton = (Button)findViewById(R.id.setreminder);
     //   dbReminder = new ReminderDb(ConversationActivity.this);
        SetReminderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //remindertext = reminderbody.getText().toString();

         /*     Cursor data = dbManager.readData();
              data.moveToLast();
              boolean insertcheck = dbReminder.addData(data.getString(0),remindertext);     */
              startActivity(new Intent(ConversationActivity.this,reminder.class));
              Intent intent = new Intent(ConversationActivity.this,reminder.class);
              intent.putExtra(Constant.KEY_REMINDER_MESSAGE,reminderbody.getText().toString());
              startActivity(intent);
            }
        });


        //Set the next button.
        Button yesButton = (Button)findViewById(R.id.yes_btn);
        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(FloatingViewService.this, "selected yes.", Toast.LENGTH_LONG).show();
                boolean checking = isPermissionGranted();
                if (checking)
                    makecall();
                else {
                    maincall();
                    makecall();
                }
                startService(new Intent(ConversationActivity.this,FloatingViewService.class));
                expandedView.setVisibility(View.VISIBLE);
            }
        });
       /* View reminder = (View) findViewById(R.id.remindermessage);
        TextView notify = (TextView) findViewById(R.id.reminderalert);
        if(getCallingActivity().toString().equalsIgnoreCase("myalarm.java")){
            Cursor data = dbReminder.readData();
            data.moveToLast();
            notify.setText(data.getString(1));
            reminder.setVisibility(View.VISIBLE);

        };
        */
        CallDetails cd;
        cd = CallDetails.getLastCallDetails(ConversationActivity.this);
        dbManager = new DbManager(ConversationActivity.this);
        name = (TextView) this.findViewById(R.id.question);
        addcheck = dbManager.addData(cd.cname,cd.cduration,cd.cdate,cd.phoneno,cd.ctype);
        data = dbManager.readData();
        if (data.moveToLast()) {
            do {
           /* if(data.getString(2).equalsIgnoreCase("incoming"))
                name.setText(inc+data.getString(1)+ business);
            else
                name.setText(out+data.getString(1)+business);
            type.setText(data.getString(2));*/
                if (data.getString(0) != null)
                    finalstring = que + data.getString(0) + business;
                else
                    finalstring = que + "achuth" + business;
                name.setText(finalstring);
            } while (data.moveToPrevious());

        }

        reminderalert = (RelativeLayout) findViewById(R.id.remindermessage);
        notify = (TextView)findViewById(R.id.reminderalert);


        }
        //….


  private void sendSMS(String phoneNumber, String message) {
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, null, null);
    }
    boolean  maincall() {
        return false;
    }

    /**
     * Detect if the floating view is collapsed or expanded.
     *
     * @return true if the floating view is collapsed.
     */
    private boolean isViewCollapsed() {
        return mFloatingView == null || mFloatingView.findViewById(R.id.collapse_view).getVisibility() == View.VISIBLE;
    }



    private boolean isTelephonyEnabled(){
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
        return telephonyManager != null && telephonyManager.getSimState()==TelephonyManager.SIM_STATE_READY;
    }

    //function to make call to last person in call log
    void makecall(){

        Cursor data = dbManager.readData();
        data.moveToFirst();
        boolean check=isTelephonyEnabled();
        if (check)
        {Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:"+data.getString(2)));
            try{startActivity(callIntent);}
            catch (SecurityException e){
                maincall();
                makecall();
                Toast.makeText(this, "allow call permission", Toast.LENGTH_LONG).show();
            }}
    }

}
