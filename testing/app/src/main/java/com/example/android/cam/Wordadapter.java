package com.example.android.cam;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class Wordadapter extends ArrayAdapter<words> {

    public Wordadapter(@NonNull Context context, int resource, @NonNull List<words> word) {
        super(context, 0, word);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Check if the existing view is being reused, otherwise inflate the view
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_view, parent, false);
        }

        // Get the object located at this position in the list
        words currentWord = getItem(position);
        TextView cname = (TextView) listItemView.findViewById(R.id.cust_name);
        cname.setText(currentWord.getWcustomer());
        TextView ctype = (TextView) listItemView.findViewById(R.id.call_type);
        ctype.setText(currentWord.getWtype());
        TextView cnumber=(TextView)listItemView.findViewById(R.id.number);
        cnumber.setText(currentWord.getWnumber());
        TextView cduration=(TextView)listItemView.findViewById(R.id.durartion);
        cduration.setText(currentWord.getWduration());
        TextView cdate=(TextView)listItemView.findViewById(R.id.date);
        cdate.setText(String.valueOf(currentWord.wdate));

        return listItemView;
    }
}
