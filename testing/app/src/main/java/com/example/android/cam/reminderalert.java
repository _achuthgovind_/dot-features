package com.example.android.cam;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.cam.utils.Constant;

public class reminderalert extends AppCompatActivity {
    Cursor data;
    ReminderDb db ;
    TextView notify,name;
    RelativeLayout reminderalert;
    long time;
    Button okay;
    static boolean active = false;

    @Override
    public void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        active = false;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_conversation);
        setContentView(R.layout.reminderlist);
        okay = (Button) findViewById(R.id.okayreminder);
        name =(TextView)findViewById(R.id.reminderfor);
        notify = (TextView)findViewById(R.id.reminderalert);
        reminderalert = (RelativeLayout) findViewById(R.id.remindermessage);
        db = new ReminderDb(this);
        Toast.makeText(this,"inside reminderalert",Toast.LENGTH_LONG);
        getExtra();
        data = db.readData(time);
        data.moveToFirst();

        notify.setText(data.getString(1));

        name.setText(data.getString(0));
        //Toast.makeText(this,notify.getText(),Toast.LENGTH_LONG);

        reminderalert.setVisibility(View.VISIBLE);


    }
    void getExtra(){
       time = getIntent().getLongExtra(Constant.KEY_ALARM_TIME,0);
    }
    void next(final long nexttime){
        okay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                    data = db.readData(nexttime);
                    data.moveToFirst();
                    notify.setText(data.getString(1));
                    name.setText(data.getString(0));
                }
            }
        );
    }
}
