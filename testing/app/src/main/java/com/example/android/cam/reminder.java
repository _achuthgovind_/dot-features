package com.example.android.cam;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.android.cam.utils.Constant;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class reminder extends AppCompatActivity {
    //the timepicker object
    TimePicker timePicker;

    List<PendingIntent> arrayListAlaram = new ArrayList<>();
    static int lastPendingIntent =0;

    String reminderMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timeset);

        getExtra();
        final ReminderDb db = new ReminderDb(this);
        final DbManager dbmanager = new DbManager(this);
        final ConversationActivity ca = new ConversationActivity();
        //getting the timepicker object
        timePicker = (TimePicker) findViewById(R.id.timePicker);
        View reminderview = LayoutInflater.from(this).inflate(R.layout.activity_conversation, null);
        EditText reminder=(EditText)reminderview.findViewById(R.id.remindertext);
                //attaching clicklistener on button
        findViewById(R.id.buttonAlarm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //We need a calendar object to get the specified time in millis
                //as the alarm manager method takes time in millis to setup the alarm
                Calendar calendar = Calendar.getInstance();


                if (android.os.Build.VERSION.SDK_INT >= 23) {
                    calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH),
                            timePicker.getHour(), timePicker.getMinute(), 0);
                } else {
                    calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH),
                            timePicker.getCurrentHour(), timePicker.getCurrentMinute(), 0);
                }
                long currentTime = calendar.getTimeInMillis();
                Cursor data =dbmanager.readData();
                data.moveToFirst();
                while(data.moveToNext())
                {
                    if(data.getLong(2)-currentTime<60000)

                }
                 data = dbmanager.readData();
                data.moveToNext();
                boolean insertcheck = db.addData(data.getString(0),reminderMessage,currentTime);
                setAlarm(currentTime);

                startActivity(new Intent(getApplicationContext(),ConversationActivity.class));
            }
        });
    }

    private void getExtra(){

        reminderMessage = getIntent().getStringExtra(Constant.KEY_REMINDER_MESSAGE);
    }


    private void setAlarm(long time) {
        //getting the alarm manager
        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        //creating a new intent specifying the broadcast receiver
        Intent i = new Intent(this, MyAlarm.class);
        i.putExtra(Constant.KEY_ALARM_TIME,time);
        //creating a pending intent using the intent
        PendingIntent pi = PendingIntent.getBroadcast(this,lastPendingIntent, i, 0);

        //alarm that will be fired
        //am.set(am.RTC_WAKEUP,time,pi);
        am.setRepeating(AlarmManager.RTC, time, AlarmManager.INTERVAL_DAY, pi);

        lastPendingIntent++;

        //arrayListAlaram.add(pi);
        Toast.makeText(this, "Alarm is set", Toast.LENGTH_SHORT).show();
    }
}

