package com.example.android.cam;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.cam.utils.Constant;

//class extending the Broadcast Receiver
public class MyAlarm extends BroadcastReceiver {
    reminderalert ra;
    long time;
    Context temp;

    @Override
    public void onReceive(Context context, Intent intent) {
       // MediaPlayer mp = MediaPlayer.create(context, Settings.System.DEFAULT_NOTIFICATION_URI);
        //mp.start();
        temp = context;
        getExtra(intent);
        Toast.makeText(context,"in myalarm",Toast.LENGTH_LONG);

        Intent i = new Intent(context,reminderalert.class);
        i.putExtra(Constant.KEY_ALARM_TIME,time);

        Log.e("MyAlarmBelal", "Alarm just fired");
        ra = new reminderalert();
        if(reminderalert.active){
            ra.next(time);
        }
        else
            context.startActivity(i);

    }
    private void getExtra(Intent intent){
        if(intent.getExtras().containsKey(Constant.KEY_ALARM_TIME))
        time =intent.getLongExtra(Constant.KEY_ALARM_TIME,0);
        else
            Toast.makeText(temp,"time not found",Toast.LENGTH_LONG);
    }

}