package com.example.android.cam;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class RWordAdapter extends ArrayAdapter<rwords> {

    public RWordAdapter(@NonNull Context context, int resource, @NonNull List<rwords> rword) {
        super(context, 0, rword);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Check if the existing view is being reused, otherwise inflate the view
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.reminderlist, parent, false);
        }

        // Get the object located at this position in the list
        rwords currentWord = getItem(position);
        TextView name = (TextView) listItemView.findViewById(R.id.reminderfor);
        name.setText(currentWord.getName());
        TextView body = (TextView) listItemView.findViewById(R.id.reminderalert);
        body.setText(currentWord.getRemindermessage());

        return listItemView;
    }
}
