package com.example.android.cam;

import android.database.Cursor;

public interface BindCallData {

    void callBack(Cursor cursor);
}
